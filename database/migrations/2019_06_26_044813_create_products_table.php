<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->Increments('id');
			$table->unsignedInteger('cat_id');
			$table->foreign('cat_id')->references('id')->on('categories');
			$table->string('product_name')->nullable();
			$table->string('product_desc')->nullable();
			$table->string('price')->nullable();
			$table->string('quantity')->nullable();
			$table->string('total_price')->nullable();
			$table->enum('status', ['0','1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
