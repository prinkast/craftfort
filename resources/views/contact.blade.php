@extends('layouts.main')
@section('content')
<!-----contact---->
    <div class="contact_div">
       <div class="container">
           <h2>Contact us</h2>
          <div class="contact_inner">
            <form method="post">
			@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
			@csrf
                <div class="from-group">
                  <label>First Name</label>
                    <input type="text" name="firstname" class="frm_in form-control">        
                </div>
                      <div class="from-group">
                  <label>Last Name</label>
                    <input type="text" name="lastname" class="frm_in form-control">        
                </div>
              
                     <div class="from-group">
                  <label>Email</label>
                    <input type="email" name="email" class="frm_in form-control">        
                </div>
                   <div class="from-group">
                  <label>Address</label>
                    <input type="text" name="address" class="frm_in form-control">        
                </div>
                   <div class="from-group">
                  <label>Number </label>
                    <input type="number" name="number" class="frm_in form-control">        
                </div>
                <div class="from-group">
                  <label>Pincode</label>
                    <input type="number" name="pincode" class="frm_in form-control">        
                </div>
                  <div class="from-group from-group1">
                  <label>Comment </label>
                    <textarea name="comment" class="txt_in"></textarea> 
                      
                </div>
                
                <input class="sub" name="submit" type="submit" value="Submitted">
                
                
                
              </form>
              
             
           
           </div>
        
        
        </div>
    
    </div>
	
	
	 
            <div class="address">
              <div class="">
             
             
                     
                   
                  <div class="addres_inner">
                      <img src="{{asset('public/images/logo2.png')}}">
                     <h6>SCF 37, Top Floor, Phase 10, Mohali(PB) INDIA</h6>
                     <ul>
                        <li>Email:<a href="#">craftfort@gmail.com</a></li>
                          <li>Phone:<a href="#">+91 172 4188 449 </a>
                         <br>
                         <a href="#">+91 9115 003 846</a></li>
                      </ul>
                  </div>
                
                     
                   
                        <div class="map">
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3431.1674893072363!2d76.73728441509058!3d30.685563294955493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390fec1b89c79fb1%3A0xfacc69973430568c!2sShail+Tech!5e0!3m2!1sen!2sin!4v1561533109080!5m2!1sen!2sin" width="100%" height="460" frameborder="0" style="border:0" allowfullscreen></iframe>
                         
                         </div>
                    
                  
                  </div>
             
           </div>
           <!----address--->
        
    
    
    
		   
<!-----contact---->
@endsection
