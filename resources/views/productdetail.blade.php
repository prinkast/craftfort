@extends('layouts.main')
@section('content')
    
    
<!---product_div---->
    <div class="product_div">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
<section class="content1">
  <article class="demo-area">
  
   @foreach(json_decode($product) as $produc)
   <?php
		 $products= $produc;
		// $img = json_decode($products->image);
		$image = $products->image;
		  ?>
		  
			<div class="slider_product">
			  <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
 @if(count($image) > 0)
	 <?php $a=1;?>
  @foreach(json_decode($image) as $images)
  
  
    <div class="item slider2 <?php if($a==1){?> active <?php } ?>"  >
	 
       <img src="{{URL::asset('/public/products/'.$images)}}" height="200px" width="100px"></a>
	  
    </div>
	  <?php $a++;?>
 @endforeach
  @endif
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
			
			
			</div>
			
  
 
  
  
  
  
  <div class="detail detail_pro">
   <section>
      <h3>{{$products->product_name}}</h3>
     
      <ul>
        <li>Brands <span>Craftfort</span></li>
        <li>Product Code<span>Product 19</span></li>
        <li>Availability: <span> In Stock</span></li>
		 <li><h3>{{$products->product_desc}}</h3></li>
      </ul>
      <h4>₹{{$products->sell_price}}</h4>
	  
	  
       <form>
           <label>QTY</label>
       <input type="number" class="num" placeholder="1" value="{{$products->quantity}}">
           <a href="javascript:void(0)" onclick="addtoCart('{{ $products->id }}')"><i class="fa fa-signal" aria-hidden="true"></i> Add To Cart</a>
       </form>
       
       
       
       <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Add To Wishlist</a>   
       <a href="#"><i class="fa fa-signal" aria-hidden="true"></i> Add To Compare</a>
    </section>
  </div>
   @endforeach  
  </article>
</section>


    
            </div>  
          </div>
        </div>
             
    </div>
	
	  <script>
    var demoTrigger = document.querySelector('.demo-trigger');
var paneContainer = document.querySelector('.detail');

new Drift(demoTrigger, {
  paneContainer: paneContainer,
  inlinePane: false,
});
    </script>
<!---product_div---->
    
   
    
@endsection