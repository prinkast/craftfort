@extends('backend.layouts.main')

@section('content')
		<!-- END: Header -->
	
			<!-- END: Left Aside -->
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="page-header">
							<div class="container-fluid">
								<div class="pull-right">
									<!--button type="button" data-toggle="tooltip" class="btn btn-default hidden-md hidden-lg"><i class="fa fa-filter"></i>
									</button-->
									<a type="button" class="btn btn-primary"  href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>
									</a>
									<!--button type="submit" form="form-product" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Copy"><i class="fa fa-copy"></i>
									</button>
									<button type="button" form="form-product" data-toggle="modal" data-target="#myModal1" title="" class="btn btn-danger"><i class="fa fa-trash-o"></i>
									</button-->
                               
								</div>
								<h1>Products</h1>
								
							</div>
						
						<div class="container-fluid">
							<div class="row">
							<div class="col-md-12 col-sm-12">
									<div class="panel panel-default">
										
										<div class="panel-body">
											<form id="form-product">
												<div class="table-responsive">
												
													<table class="table table-bordered table-hover" id="table_product">
														<thead>
															<tr>
																<td style="width: 1px;" class="text-center" >
																	<input type="checkbox">
																</td>
																
																<td class="text-left"> <a href="#" class="asc">Category</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Product Name</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Product Desc</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc"> Original Price</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc"> Sell Price</a> 
																</td>
																<td class="text-left"> <a href="#">Quantity</a> 
																</td>
																<td class="text-left"> <a href="#">Total Price</a> 
																</td>
																<td class="text-left"> <a href="#">Status</a> 
																</td>
																<!--td class="text-left"> <a href="#">Image</a> 
																</td-->
																<td class="text-left">Action</td>
															</tr>
														</thead>
														<tbody>
															
              
															
														
															
														</tbody>
													</table>
												</div>
											</form>
											<div class="row">
												<div class="col-sm-6 text-left">
													<ul class="pagination">
														<li class="active"><span>1</span>
														</li>
														<li><a href="#">2</a>
														</li>
														<li><a href="#">&gt;</a>
														</li>
														<li><a href="#">&gt;|</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-6 text-right">Showing 1 to 20 of 21 (2 Pages)</div>
											</div>
										</div>
									</div>
								</div>
								<!--div id="filter-product" class="col-md-3 col-md-push-9 col-sm-12 hidden-sm hidden-xs">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title"><i class="fa fa-filter"></i> Filter</h3>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<label class="control-label" for="input-name">Product Name</label>
												<input type="text" name="filter_name" value="" placeholder="Product Name" id="input-name" class="form-control" autocomplete="off">
												<ul class="dropdown-menu"></ul>
											</div>
											<div class="form-group">
												<label class="control-label" for="input-model">Model</label>
												<input type="text" name="filter_model" value="" placeholder="Model" id="input-model" class="form-control" autocomplete="off">
												<ul class="dropdown-menu"></ul>
											</div>
											<div class="form-group">
												<label class="control-label" for="input-price">Price</label>
												<input type="text" name="filter_price" value="" placeholder="Price" id="input-price" class="form-control">
											</div>
											<div class="form-group">
												<label class="control-label" for="input-quantity">Quantity</label>
												<input type="text" name="filter_quantity" value="" placeholder="Quantity" id="input-quantity" class="form-control">
											</div>
											<div class="form-group">
												<label class="control-label" for="input-status">Status</label>
												<select name="filter_status" id="input-status" class="form-control">
													<option value=""></option>
													<option value="1">Enabled</option>
													<option value="0">Disabled</option>
												</select>
											</div>
											<div class="form-group text-right">
												<button type="button" id="button-filter" class="btn btn-default"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div-->
								
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
				</div>
			</div>
			<!--------end -UNDER-dashboard------------------->
		</div>

	</div>
	
	<!---SCRIPT---->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<script src="public/js/scripts.bundle.js"></script>
	<script src="public/js/vendors.bundle.js"></script>
    
    
	
    
    
    
	<!-----popup------>
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="page-header">
						<div class="container-fluid">
							<!--div class="pull-right">
								<button type="submit" form="form-product" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Save"><i class="fa fa-save"></i>
								</button> <a href="#" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
							</div-->
							
							
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div id="content">

						<div class="container-fluid">
							<div class="panel panel-default">
								<!--div class="panel-heading">
									<h3 class="panel-title"><i class="fa fa-pencil"></i> Add Product</h3>
								</div-->
								<div class="panel-body">
									<h1>Add Products</h1>
									 <form id="data" name="productForm" enctype="multipart/form-data" class="form-horizontal">
									 @csrf
                   <input type="hidden" name="product_id" id="product_id">
				   <div class="form-group">
                        
                        <!--div class="col-sm-12">
                            <input type="text" id="cat_id" name="cat_id"  placeholder="Enter Details" class="form-control">
                        </div!-->
                            <?php $cat = App\Category::CategoryList();
                            $cat_data= json_decode($cat);?>
                            
                            
                            <label class="col-sm-2 control-label">Category Name</label>
                            <select id="cat_id" name="cat_id">
                            @foreach($cat_data as $cats)
                            <option id="cat_id" name="cat_id" value="{{$cats->id}}">{{$cats->cat_name}}</option>
                            @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Product Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="product_name" name="product_name" placeholder="product name" value="" maxlength="50" required="">
                        </div>
                    </div>
     
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Product Description</label>
                        <div class="col-sm-12">
                          <textarea type="text" id="product_desc" name="product_desc" required="" placeholder="Enter product desc" class="form-control"></textarea>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label">Original price</label>
                        <div class="col-sm-12">
                           <input type="numbers" id="price" name="price" required="" placeholder="Enter original price" class="form-control">
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="col-sm-2 control-label">Sell price</label>
                        <div class="col-sm-12">
                           <input type="numbers" id="sell_price" name="sell_price" required="" placeholder="Enter sell price" class="form-control">
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="col-sm-2 control-label">Quantity</label>
                        <div class="col-sm-12">
                          <input type="numbers" id="quantity" name="quantity" required="" placeholder="Enter quantity" class="form-control">
                        </div>
                    </div>
					<div class="form-group">
                        
                        <div class="col-sm-12">
                          <input type="hidden" id="total_price" name="total_price" required="" placeholder="Enter price" class="form-control">
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-12">
                           <select name="status" id="status" class="form-control ">
                              <option value="ACTIVE" selected="selected">ACTIVE</option>
                              <option value="INACTIVE" >INACTIVE</option>
                          </select>
                        </div>
                    </div>
				<div class="form-group">	
				<label class="col-sm-2 control-label">Upload Image</label>
        <input type="file" name="image[]" id="image"  multiple class="form-control">
		 </div>
                    <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                     </button>
                    </div>
                </form>
									
									
									
								</div>
							</div>
						</div>
					</div>
                </div>
            
					
				</div>
			</div>
		</div>
    
    
<!----scd---pupup----->
    
    <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
 
          <div class="delet">
             <h6>www.Craftfort.com says</h6>
              <p>Are you sure?</p>
            
            </div>
     
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancle</button>
        </div>
      </div>
      
    </div>
  </div>
  
	 <script type="text/javascript">
  $(function() {
	  
	   $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
               $('#table_product').DataTable({
               processing: true,
               serverSide: true,
               ajax: '{{ url('admin/product_index') }}',
               columns: [
                        { data: 'id', name: 'id' },
                        { data: 'cat_name', name: 'cat_id' },
                        { data: 'product_name', name: 'product_name' },
                        { data: 'product_desc', name: 'product_desc' },
                        { data: 'price', name: 'price' },
                        { data: 'sell_price', name: 'sell_price' },
                        { data: 'quantity', name: 'quantity' },
                        { data: 'total_price', name: 'total_price'},
                        { data: 'status', name: 'status' },
                      /*  {
				data: 'image',
				"render": function(data, type, row) {
					return '<img src="http://craftfort.localhost/public/products/'+data+'" style="height:100px;width:100px;" />';
				} 
			}, */
			 {data: 'action', name: 'action', orderable: false, searchable: false},
                      
                     ]
            });
       
		 
		  $("form#data").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);

    $.ajax({
       url: '{{ url('admin/product_store') }}',
        type: 'POST',
        data: formData,
        success: function (data) {
           
        },
        cache: false,
        contentType: false,
        processData: false,
		 success: function (data) {
     
              $('#productForm').trigger("reset");
              $('#ajaxModel').modal('hide');
			   window.location.reload();
              table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
    });
		  });
	
	 $('body').on('click', '.deleteProduct', function () {
     
        var product_id = $(this).data("id");
		//alert(product_id);
        //confirm("Are You sure want to delete !");
      
        $.ajax({
            type: "GET",
			
           url: '{{ url('admin/product_delete') }}'+'/'+product_id,
            success: function (data) {
				 window.location.reload();
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
	
	  $('body').on('click', '.editProduct', function () {
      var product_id = $(this).data('id');
	  
      $.get('{{ url('admin/product_edit') }}' +'/' + product_id , function (data) {
	
          $('#modelHeading').html("Edit Product");
          $('#saveBtn').val("edit-user");
          $('#product_id').val(data.id);
          $('#cat_id').val(data.cat_id);
          $('#product_name').val(data.product_name);
          $('#product_desc').val(data.product_desc);
          $('#price').val(data.price);
          $('#sell_price').val(data.sell_price);
          $('#quantity').val(data.quantity);
          $('#total_price').val(data.total_price);
          $('#status').val(data.status);
          $("#ajaxModel").modal('show');
		  
         
        
      })
   });

  });
</script>
				@endsection	 