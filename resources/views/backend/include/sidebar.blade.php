	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
			<!-- BEGIN: Left Aside -->
			<!-----------------------side_bar------------------------->
			<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
				<!-- BEGIN: Aside Menu -->
				<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
					<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                     
                        
                        
                        <li class="m-menu__item " aria-haspopup="true"><a href="/admin" class="m-menu__link "> <i class="fa fa-dashboard dash_in  m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">Dashboard</span>      <span class="m-menu__link-badge"><!--span class="m-badge m-badge--danger">2</span--></span>  </span></span></a></li>
                        
						<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="/ajaxcategory" class="m-menu__link m-menu__toggle"><i class="fa fa-list dash_in m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-text">Categories</span><!--i class="fa fa-angle-down art_in" aria-hidden="true"></i--></a>
							<!--div class="m-menu__submenu "><span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">Categories</span></span>
									</li>
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-plus"></i>&nbsp;Add</span></a>
									</li>
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-edit"></i>&nbsp;Edit</span></a>
									</li>
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-trash"></i>&nbsp;Delete</span></a></li>
									
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-eye"></i>&nbsp;View</span></a></li>
                             
								</ul>
							</div-->
						</li>
                        
                        
						<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="/admin/product_index" class="m-menu__link m-menu__toggle"><i class="fa fa-cubes dash_in m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-text">Products</span><!--i class="fa fa-angle-down art_in" aria-hidden="true"></i--></a>
							<!--div class="m-menu__submenu "><span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<!li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">Products</span></span>
									</li>
																	<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-plus"></i>&nbsp;Add</span></a>
									</li>
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-edit"></i>&nbsp;Edit</span></a>
									</li>
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-trash"></i>&nbsp;Delete</span></a></li>
									
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-eye"></i>&nbsp;View</span></a></li>
                             
								</ul>
							</div-->
						</li>
                        
                        
                        
						<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="/ajaxcoupan" class="m-menu__link m-menu__toggle"><i class="fa fa-cc dash_in m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-text">Coupons</span><!--i class="fa fa-angle-down art_in" aria-hidden="true"></i--></a>
							<!--div class="m-menu__submenu "><span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">Coupons</span></span>
									</li>
																	<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-plus"></i>&nbsp;Add</span></a>
									</li>
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-edit"></i>&nbsp;Edit</span></a>
									</li>
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-trash"></i>&nbsp;Delete</span></a></li>
									
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-eye"></i>&nbsp;View</span></a></li>
                             
								</ul>
							</div-->
						</li>
                      
                        
						<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="#" class="m-menu__link m-menu__toggle"><i class="fa fa-cart-arrow-down dash_in m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-text">Orders</span></a>
						</li>
                        
                        
                        
						<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="/admin/customer_index" class="m-menu__link m-menu__toggle"><i class="fa fa-users dash_in m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-text">Customers</span></a>	
						</li>
                        
                        
						<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="/admin/uploadimage_index" class="m-menu__link m-menu__toggle"><i class="fa fa-file-image-o dash_in m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-text">Slider & Logo</span></a>
						</li>
                 
                        
                        
						<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="#" class="m-menu__link m-menu__toggle"><i class="fa fa-graduation-cap dash_in m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-text">Product colors</span></a></li>
						
												<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="#" class="m-menu__link m-menu__toggle"><i class="fa fa-user-circle-o dash_in m-menu__link-icon" aria-hidden="true"></i><span class="m-menu__link-text">My Account</span><i class="fa fa-angle-down art_in" aria-hidden="true"></i></a>
							<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">My Account</span></span>
									</li>
														
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-cog"></i>&nbsp;Settings</span></a></li>
									
									<li class="m-menu__item " aria-haspopup="true"><a href="#" class="m-menu__link "><span class="m-menu__link-text"><i class="fa fa-sign-out"></i>&nbsp;Logout</span></a></li>
                             
								</ul>
							</div>
						</li>
                        
                        
						
					</ul>
				</div>
				<!-- END: Aside Menu -->
                
                
                
                
			</div>
            	<!-----------------------end-side_bar------------------------->
            
            
            <!--------UNDER-dashboard------------------->
			<!-- END: Left Aside -->