@extends('backend.layouts.main')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Dashboard</h3>	
                            <ul class="dash_in">
                              <li>Home</li>
                                <li><a href="#">/ Dashboard</a></li>
                            </ul>
							
                            <div class="box_div">
                               <div class="row">
                                  <div class="col-sm-3">
                                   <div class="tile tile-primary">
                          <div class="tile-heading">Total Orders <span class="pull-right">
                                0%</span></div>
                          <div class="tile-body"><i class="fa fa-shopping-cart"></i>
                            <h2 class="pull-right">9</h2>
                          </div>
                          <div class="tile-footer"><a href="#">View more...</a></div>
                        </div>
                                   
                                   
                                   </div>
                                   
                                   <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile tile-primary">
                                          <div class="tile-heading">Total Sales <span class="pull-right">
                                                0%</span></div>
                                          <div class="tile-body"><i class="fa fa-credit-card"></i>
                                            <h2 class="pull-right">2.3K</h2>
                                          </div>
                                          <div class="tile-footer"><a href="https://www.craftfort.com/admin/index.php?route=sale/order&amp;user_token=5EMAIQ1VJX6KJeMIwh4fIp5hJX765LfW">View more...</a></div>
                                        </div>
                                    </div>
                                   
                                   <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile tile-primary">
                                      <div class="tile-heading">Total Customers <span class="pull-right">
                                            0%</span></div>
                                      <div class="tile-body"><i class="fa fa-user"></i>
                                        <h2 class="pull-right">8</h2>
                                      </div>
                                      <div class="tile-footer"><a href="https://www.craftfort.com/admin/index.php?route=customer/customer&amp;user_token=5EMAIQ1VJX6KJeMIwh4fIp5hJX765LfW">View more...</a></div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6"><div class="tile tile-primary">
                                      <div class="tile-heading">People Online</div>
                                      <div class="tile-body"><i class="fa fa-users"></i>
                                        <h2 class="pull-right">4</h2>
                                      </div>
                                      <div class="tile-footer"><a href="https://www.craftfort.com/admin/index.php?route=report/online&amp;user_token=5EMAIQ1VJX6KJeMIwh4fIp5hJX765LfW">View more...</a></div>
                                    </div>
                                </div>
                                
                                </div>
						</div>
                            
                            
                            <div class="map_div">
                                 <div class="row">
                                     <div class="col-sm-6">
                                    <div class="map_inner">
                                         <h3> <i class="fa fa-globe" aria-hidden="true"></i>World Map</h3>
                                        <div id="vmap" style="width: 100%; height: 400px;"></div>
                                     </div>
                                     
                                </div>
                                     
                                     <div class="col-sm-6">
                                         <div class="map_inner">
                                           <h3><i class="fa fa-bar-chart-o"></i> Sales Analytics</h3>
                                           <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                                             
                                         </div>
                                         
                                     </div>
                                </div>
                            </div>
                            
                            
                            <div class="tab_div">
                                <div class="row">
                                   <div class="col-lg-4 col-md-12 col-sm-12">
                                    <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <h3 class="panel-title"><i class="fa fa-calendar"></i> Recent Activity</h3>
                                      </div>
                                      <ul class="list-group">
                                                <li class="list-group-item"><a href="#">arjun sharma</a> logged in.<br>
                                          <small class="text-muted"><i class="fa fa-clock-o"></i> 25/06/2019 10:39:21</small></li>
                                            <li class="list-group-item"><a href="#">arjun sharma</a> reset their account password.<br>
                                          <small class="text-muted"><i class="fa fa-clock-o"></i> 25/06/2019 10:39:11</small></li>
                                            <li class="list-group-item"><a href="#">arjun sharma</a> has requested a reset password.<br>
                                          <small class="text-muted"><i class="fa fa-clock-o"></i> 25/06/2019 10:33:28</small></li>
                                            <li class="list-group-item"><a href="#">arjun sharma</a> has requested a reset password.<br>
                                          <small class="text-muted"><i class="fa fa-clock-o"></i> 25/06/2019 10:31:58</small></li>
                                            <li class="list-group-item"><a href="#">PARMINDER KUMAR Dhawan</a> logged in.<br>
                                          <small class="text-muted"><i class="fa fa-clock-o"></i> 01/05/2019 15:38:19</small></li>
                                              </ul>
                                       </div>
                                    </div> 
                                    
                                    
                                    <div class="col-lg-8 col-md-12 col-sm-12"><div class="panel panel-default">
                                          <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Latest Orders</h3>
                                          </div>
                          <div class="table-responsive">
                            <table class="table">
                              <thead>
                                <tr>
                                  <td class="text-right">Order ID</td>
                                  <td>Customer</td>
                                  <td>Status</td>
                                  <td>Date Added</td>
                                  <td class="text-right">Total</td>
                                  <td class="text-right">Action</td>
                                </tr>
                              </thead>
                              <tbody>
                                                <tr>
                                  <td class="text-right">19</td>
                                  <td>PARMINDER KUMAR Dhawan</td>
                                  <td>Pending</td>
                                  <td>30/04/2019</td>
                                  <td class="text-right">₹625.40</td>
                                  <td class="text-right"><a href="https://www.craftfort.com/admin/index.php?route=sale/order/info&amp;user_token=5EMAIQ1VJX6KJeMIwh4fIp5hJX765LfW&amp;order_id=19" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a></td>
                                </tr>
                                        <tr>
                                  <td class="text-right">17</td>
                                  <td>Gaurav Dhawan</td>
                                  <td>Pending</td>
                                  <td>29/04/2019</td>
                                  <td class="text-right">₹418.90</td>
                                  <td class="text-right"><a href="https://www.craftfort.com/admin/index.php?route=sale/order/info&amp;user_token=5EMAIQ1VJX6KJeMIwh4fIp5hJX765LfW&amp;order_id=17" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a></td>
                                </tr>
                                        <tr>
                                  <td class="text-right">16</td>
                                  <td>Parminder Kumar Dhawan</td>
                                  <td>Pending</td>
                                  <td>10/04/2019</td>
                                  <td class="text-right">₹625.40</td>
                                  <td class="text-right"><a href="https://www.craftfort.com/admin/index.php?route=sale/order/info&amp;user_token=5EMAIQ1VJX6KJeMIwh4fIp5hJX765LfW&amp;order_id=16" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a></td>
                                </tr>
                                        <tr>
                                  <td class="text-right">15</td>
                                  <td>test demo</td>
                                  <td>Failed</td>
                                  <td>16/02/2019</td>
                                  <td class="text-right">₹212.40</td>
                                  <td class="text-right"><a href="https://www.craftfort.com/admin/index.php?route=sale/order/info&amp;user_token=5EMAIQ1VJX6KJeMIwh4fIp5hJX765LfW&amp;order_id=15" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a></td>
                                </tr>
                                        <tr>
                                  <td class="text-right">14</td>
                                  <td>test demo</td>
                                  <td>Failed</td>
                                  <td>16/02/2019</td>
                                  <td class="text-right">₹230.10</td>
                                  <td class="text-right"><a href="https://www.craftfort.com/admin/index.php?route=sale/order/info&amp;user_token=5EMAIQ1VJX6KJeMIwh4fIp5hJX765LfW&amp;order_id=14" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a></td>
                                </tr>
                                              </tbody>
                            </table>
                          </div>
</div>
</div>
                                    
                                    
                                    
                                </div>
                            </div>
                            
                            
					
					</div>
				</div>
				<!-- END: Subheader -->
				
			</div>
		</div>
            <!--------end -UNDER-dashboard------------------->
            
            
            
    </div>
			<!------------------main_div--------------->
			@endsection