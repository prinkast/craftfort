@extends('backend.layouts.main')

@section('content') 
        
		 <div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="page-header">
							<div class="">
								<div class="pull-right"><a href="#" data-toggle="modal" title="" class="btn btn-primary" data-original-title="Add New" ><i class="fa fa-plus" id="createNewcat"></i></a>
									<!--a href="#" data-original-title="Rebuild" class="btn btn-default"><i class="fa fa-refresh"></i></a>
									<button type="button" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i>
									</button-->
								</div>
								<h1>Categories</h1>
								<!--ul class="breadcrumb">
									<li><a href="#">Home</a>
									</li>
									<li><a href="#">Categories</a>
									</li>
								</ul-->
							</div>
						</div>
						@if(session()->has('message'))
							{{ session()->get('message') }}
						@endif
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-list"></i> Category List</h3>
							</div>
							<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-bordered table-hover" id="table">
											<thead>
												<tr>
													<!--td style="width: 1px;" class="text-center">
														<input type="checkbox">
													</td!-->
													<td class="text-left"> <a href="#" class="asc">Category ID</a></td>
													<td class="text-left"> <a href="#" class="asc">Category Name</a></td>
													<td class="text-left"> <a href="#" class="asc">Category Details</a></td>
													<td class="text-left"> <a href="#" class="asc">Action</a></td>
													
												</tr>
											</thead>
											<tbody>
											
											</tbody>
										</table>
									</div>
								
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
				</div>
			</div>
			<!--------end -UNDER-dashboard------------------->
		</div>
		<!------------------main_div--------------->
	<div class="offers-detail modal fade" id="ajaxModel" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
			<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
				<div class="modal-body">
					
					 <form id="categoryForm" name="categoryForm" class="form-horizontal clearfix" class="form-horizontal clearfix" action="update_Cat" method="post">
				<input type="hidden" name="cat_id" id="cat_id">
				@csrf
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="cat_name" name="cat_name" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
     
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Details</label>
                        <div class="col-sm-12">
                            <textarea id="cat_detail" name="cat_details" required="" placeholder="Enter Details" class="form-control"></textarea>
                        </div>
                    </div>
      
                     <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-primary" value="create">Save changes
                     </button>
                    </div>
                </form>
				</div>
			</div>
		</div>
	</div>
	
	
	
		<div class="offers-detail modal fade" id="ajaxModel_cat" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
			<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
				<div class="modal-body">
					
					 <form id="categorycreateForm" name="categorycreateForm" class="form-horizontal clearfix" class="form-horizontal clearfix" action="create_Cat" method="post">
				
				@csrf
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control"  name="cat_name" placeholder="Enter Name" value="" maxlength="50" required="">
                        </div>
                    </div>
     
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Details</label>
                        <div class="col-sm-12">
                            <textarea  name="cat_details" required="" placeholder="Enter Details" class="form-control"></textarea>
                        </div>
                    </div>
      
                     <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-primary" value="create">Save changes
                     </button>
                    </div>
                </form>
				</div>
			</div>
		</div>
	</div>
		@endsection	 
		 
		 
		 
		 
		 
		 
		 
       