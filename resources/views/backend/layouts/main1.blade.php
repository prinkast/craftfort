<html>
<head>
<title>Craftfort</title>
<meta charset="utf-8"> 
 
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Nobile:400,500,700|Philosopher:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/public/css/admin.css">
	<link rel="stylesheet" href="/public/css/mystyle.css">
	<link rel="icon" href="favicon.ico" type="image/icon" sizes="16x16">
	<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

</head>
<body>



@include('backend.include.header')
@include('backend.include.sidebar')
@include('backend.include.modal')
@yield('content')


@include('backend.include.footer')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		<script src="/public/js/data.js"></script>
<script>
		 $(function() {
	 
	 $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('ajaxcategory.index') }}",
        columns: [
           {data: 'DT_RowIndex', name: 'DT_RowIndex'},
			{ data: 'cat_name', name: 'cat_name' },
			{ data: 'cat_details', name: 'cat_details' },
			{data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     $('#createNewcat').click(function () {
		 
        //$('#saveBtn').val("create-product");
        //$('#cat_id').val('');
        //$('#categoryForm').trigger("reset");
        //$('#modelHeading').html("Create New Product");
        $('#ajaxModel_cat').modal('show');
    });
	$('body').on('click', '.deleteProduct', function () {
     
        var cat_id = $(this).data("id");
        confirm("Are You sure want to delete !");
      
        $.ajax({
            type: "DELETE",
            url: "{{ route('ajaxcategory.store') }}"+'/'+cat_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
	
	/* $('body').on('click', '.editProduct', function () {

      var cat_id = $(this).data('id');
	  alert(cat_id);
		  $.get("{{ route('ajaxcategory.index') }}" +'/' + cat_id +'/edit', function (data) {
          $('#modelHeading').html("Edit category");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#cat_id').val(data.id);
          $('#cat_name').val(data.cat_name);
          $('#cat_details').val(data.cat_details);
		  
      }) 
   }); */
	/* $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
    
        $.ajax({
          data: $('#categoryForm').serialize(),
          url: "{{ route('ajaxcategory.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
     
              $('#categoryForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    }); */
	
	
	
	
   });
window.onload = function() {
 
 
var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2",
	title: {
		text: "NVIDIA Corporation Stock Price - 2017"
	},
	axisX: {
		valueFormatString: "MMM",
		intervalType: "month",
		interval: 1
	},
	axisY: {
		title: "Stock Price (in USD)",
		includeZero: false,
		prefix: "$"
	},
	data: [{
		type: "ohlc",
		xValueType: "dateTime",
		yValueFormatString: "$#,##0.0",
		xValueFormatString: "MMM"
	}]
});
chart.render();
 }
 
 function openCat(id,name,details){
	 $("#cat_id").val(id);
	  $("#cat_name").val(name); 
	  $("#cat_detail").val(details);
	 $("#ajaxModel").modal('show');
 }
</script>


</body>

</html>