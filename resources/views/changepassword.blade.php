@extends('layouts.main')
@section('content')
<section class="dashboard-fileds" id="team">
   <div class="container">
   <div class="row">
     
      <div class="col-md-9">
	  <h3> Change Password</h3></br>
         <div>
            @if (session('status'))
            <div class="alert alert-success">
               {{ session('status') }}
            </div>
           
		   @endif
			
			            @if (session('status'))
							<div class="alert alert-danger"> 
						{{ session('status') }}            
						</div>           
						@endif
			
            <div class="flash-message">
               @foreach (['danger', 'warning', 'success', 'info'] as $msg) @if(Session::has('alert-' . $msg))
               <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
               @endif @endforeach
            </div>
			@if(session('errors'))
     <p class="alert alert-danger">{{session('errors')}}</p>
@endif
            
           <div> <form name="" method="POST" action="<?php echo URL::to('/changepassword/');?>" enctype="multipart/form-data">
               <input name="_token" hidden value="{!! csrf_token() !!}" />
               <div class="form-group">
                  <label>Old Password</label>
                  <div>
                     <input type="password" name="oldpassword" id="oldpassword"  required class="form-control" Placeholder=" Old Password">
                     
                  </div>
               </div>
              
               <div class="form-group">
                  <label>New Password</label>
                  <div>
                     <input type="password" name="password" id="password" required  class="form-control" Placeholder=" New Password">
                     
                  </div>
               </div>
               <div class="form-group">
                  <label>Confirm Password</label>
                  <div>
                     <input type="password" id="confirmpassword" name="confirmpassword"  required class="form-control" Placeholder="Confirm Password">
                     
                  </div>
               </div>
               
               <div class="form-group">
                  <input type="submit" name="submit" id="submit" class="btn btn-primary"  value="Submit">
               </div>
            </form></div>
         </div>
      </div>
   </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function(){
	   $("#submit").click(function(){
		var oldpassword=$("#oldpassword").val();
		var password=$("#password").val();
		var confirmpassword=$("#confirmpassword").val();
		
		
		
		 $.ajax({
			type:'post',
			url:'/changepassword',
			data:{oldpassword:oldpassword,password:password,confirmpassword:confirmpassword},
			dataType:'json',
			success:function(result){
				consol.log(result);
			}
		})
	   });	
	});
	</script>
@endsection