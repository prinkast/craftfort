@extends('layouts.main')
@section('content')

<!----CART-DIV----->
    <div class="cart_div">
        <div class="container">
            <div class="cart_inner">
              <h2>Shopping Cart</h2>
             
            	<form action="#" method="post" enctype="multipart/form-data">
                  	<div class="table-responsive cart-info">
	                    <table class="table table-bordered">
	                     	<thead>
		                        <tr>
			                        <td class="text-center">Image</td>
			                        <td class="text-center hidden-xs">Product Name</td>
			                        <td class="text-center">Quantity</td>
			                        <td class="text-center hidden-xs">Unit Price</td>
			                        <td class="text-center">Total</td>
			                        <td class="text-center">Action</td>
		                        </tr>
	                      	</thead>
	                      	<tbody>
		                      	@if($total_items == 0)
									<p style="color:black;"> Cart is Empty</p>
				
								@else

									@foreach ($items as $item)
										@foreach ($item->attributes as $attribute) 
										<?php 
										$img = json_decode($attribute['image']);
 ?>
										<tr>
				                          	<td class="text-center">            
				                              	<a href="#"><img src="{{ URL::asset('/public/products/'.$img[0])}}" class="cart_img"></a>
				                           	</td>
				                          	<td class="text-center hidden-xs">
				                              	<a href="#">{{ $item->name }}</a>
				                            </td>
				                          	<td class="text-center hidden-xs">
			                          			<input type="number" name="quantity" value="{{ $item->quantity }}"  class="num_in" id="{{ $item->id.'quan'}}">
<<<<<<< HEAD
                         						
				                          	 	
=======
<<<<<<< HEAD
                         						
				                          	 	
=======
                         						 &nbsp;
				                          	 	{{ $item->quantity }}
>>>>>>> 291d32694acc75f361be1752c882fa5852916acd
>>>>>>> 9cc800d408ce84b53db52b272c6a37bac30be335
				                          	</td>
				                          	<td class="text-center hidden-xs"> {{ $item->price }}</td>
				                          	<td class="text-center">{{ $item->price * $item->quantity }}</td>
				                          	<td class="text-center btn_cart">
				                          		<a href="javascript:void(0)" onclick="UpdateItemQuantity('{{ $item->id }}','{{ $item->id.'quan'}}')">Update</a>
				                              	<a href="javascript:void(0)" onclick="RemoveItem('{{ $item->id }}')">Remove</a>
				                          	</td>
		                    			</tr>
		                    			@endforeach
                              		@endforeach       
                            </tbody>
                              	<p style="color:black;">Total : {{ $total }}</p>
                              	@endif
                    	</table>
                  	</div>
                </form>
            </div>
        </div> 
    </div>
<!----CART-DIV----->













@endsection