@extends('layouts.app')

@section('content')
<div class="account_div2" >
        <div class="overlay"></div>
       <div class="container">
          <div class="act-inner" >

    
               
				<h2>Register</h2>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="from-group from-group2">
                            <label for="name" >{{ __('Name') }}</label>

                          
                                <input id="name" type="text" class="frm_in form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
						

                       <div class="from-group from-group2">
                            <label for="email" >{{ __('E-Mail Address') }}</label>

                            
                                <input id="email" type="email" class="frm_in form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
						 <div class="from-group from-group2">
                            <label for="phone_number" >{{ __('Phone') }}</label>

                            
                                <input id="phone_number" type="number" class="frm_in form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>

                        <div class="from-group from-group2">
                            <label for="password" >{{ __('Password') }}</label>

                          
                                <input id="password" type="password" class="frm_in form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>

                       <div class="from-group from-group2">
                            <label for="password-confirm" >{{ __('Confirm Password') }}</label>

                            
                                <input id="password-confirm" type="password" class="frm_in form-control" name="password_confirmation" required autocomplete="new-password">
                            
                        </div>
                            <div class="from-group from-group2">
                            <label for="password-confirm" ></label>

                            
                                <input type="password" class="trans">
                            
                        </div>
                     
                       <div class="from-group from-group2">
				<p>I Agree to<a href="#"> Terms and Conditions</a></p>
                                 <button class="sub_button">
                                    {{ __('Register') }}
                                </button>
                     
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
