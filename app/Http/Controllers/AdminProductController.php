<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;          
use App\Product;
use App\Product_images;
use Illuminate\Http\Request;
use Datatables;
use Illuminate\Support\Facades\Facade;
use Yajra\DataTables\Services\DataTable;

use DB;
use file;
use App\Category;
class AdminProductController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
   
       
      if ($request->ajax()) {
		  
		  $data = DB::table('products')
            ->join('categories', 'products.cat_id', '=', 'categories.id')
            ->select('products.*', 'categories.cat_name')
<<<<<<< HEAD
		
=======
<<<<<<< HEAD
		
=======
>>>>>>> 291d32694acc75f361be1752c882fa5852916acd
>>>>>>> 9cc800d408ce84b53db52b272c6a37bac30be335
            ->get();
		  
		  
			//$data1 = Product_images::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)"     data-id="'.$row->id.'" data-original-title="Edit" form="form-product" data-toggle="modal" data-target="#myModal"  class="edit btn btn-primary btn-sm editProduct">Edit</a>';
   $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('backend/productAjax');
    
      
       
    }
     
   public function store(Request $request)
    {
		$total = ($request->price) * ( $request->quantity);
		
		 $data = Product::updateOrCreate(['id' => $request->product_id],
                ['cat_id'=>$request->cat_id,'product_name' => $request->product_name, 'product_desc' => $request->product_desc,'price' => $request->price,'sell_price' => $request->sell_price, 'quantity' => $request->quantity,'total_price' => $total,'status' => $request->status
				]);  
		
		
		 $pro=   $data->id;
		 
		if($request->image!=''){
			$image=$request->image;
			
		  foreach ($image as $images) {
			  
		   $name=$images->getClientOriginalName();
									$picture_name = time().$name; 
									$images->move(public_path().'/products/', $picture_name);
								$picture[]= 	$picture_name;
									
									
									//$insert_image = dbrecords('product_images','insert',['images'=>$picture_name])->where('product_id',$request->product_id);
	
          // var_dump($directory);die;
		// move_uploaded_file($temp_name,$directory );
			}
			$pictures=json_encode($picture);
			Product_images::updateOrCreate(['product_id' => $pro],['image' => $pictures]);
		} 
		elseif(isset($request->product_id)){
			$data1=Product_images::select('image')->where('id',$pro)->first();
<<<<<<< HEAD
			var_dump($data1);die;
=======
<<<<<<< HEAD
			var_dump($data1);die;
=======
>>>>>>> 291d32694acc75f361be1752c882fa5852916acd
>>>>>>> 9cc800d408ce84b53db52b272c6a37bac30be335
		    $mnt = $data1->image;
			$pictures= $mnt;
			Product_images::updateOrCreate(['product_id' => $pro],['image' => $pictures]);
		}
		
		
 //$data = Product::select()->get();
      //  $data = Product::updateOrCreate(['id' => $request->product_id],
                //['cat_id'=>$request->cat_id,'product_name' => $request->product_name, 'product_desc' => $request->product_desc,'price' => $request->price, 'quantity' => $request->quantity,'total_price' => $request->total_price,'status' => $request->status
			//	]);  
       

          			
   
        return response()->json(['success'=>'Product saved successfully.']); 
    }
	
	 public function edit($id)
    {
        $product = Product::find($id);
        return response()->json($product);
    }
  
   
   
    public function destroy($id)
    {
		//var_dump($id);die;
        Product::find($id)->delete();
     
        return response()->json(['success'=>'Product deleted successfully.']);
    }
}
