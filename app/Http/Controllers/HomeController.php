<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Images;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
	public function indexHome()
    {
       $product = DB::table('products')
            ->join('product_images', 'products.id', '=', 'product_images.product_id')
            ->select('products.*', 'product_images.image')
            ->get();
        $images=Images::select('*')->get();
        return view('index',['product' => $product,'images'=>$images]);
    }
	
}
