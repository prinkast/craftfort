
 
 <?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
class CouponController extends Controller
{
   	public function index(){
   		return view('backend.coupons');
   	}

   	public function AddAction(Request $request){
   		if(DB::table('coupons')->insert([
			['name' => $request->name, 'discount' => $request->discount,'status'=>'0']
		])){
			return redirect()->back()->with('message','Successfuly added');
		}else{
			return redirect()->back()->with('message','Something went wrong');
		}
   	}


   	public function EditAction(Request $request, $id){
   		if(DB::table('coupons')
            ->where('id', $id)
            ->update(['name' => $request->name, 'discount' => $request->discount,'status'=>$request->status])){
   			return redirect()->back()->with('message','Successfuly added');
   		}else{
   			return redirect()->back()->with('message','Something went wrong');
   		}
    }
}