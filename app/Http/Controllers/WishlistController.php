<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Illuminate\Wishlist;
use App\Wishlists;

class WishlistController extends Controller
{
    public function __construct() {
        $this->middleware(['auth']);
    }
	
   public function index(Request $request)
    {
      $user = Auth::user();
      $wishlists =DB::table('products')
	        ->join('product_images', 'products.id', '=', 'product_images.product_id')
	        ->join('wishlists', 'products.id', '=', 'wishlists.product_id')
            ->select('products.*', 'product_images.image','wishlists.*')
            ->get();
			
		
		
		
	 // echo"<pre>";
	   // var_dump($wishlists);die;
      return view('wishlist', compact('user', 'wishlists'));
    }
	
	
	
	public function store(Request $request)
    {
		
//Validating title and body field
      $this->validate($request, array(
          'user_id'=>'required',
          'product_id' =>'required',
        ));

      $wishlist = new Wishlists;

      $wishlist->user_id =$request->user_id;
      $wishlist->product_id =$request->product_id;

//var_dump($wishlist);die;
      $wishlist->save();

      return redirect()->back()->with('flash_message',
          'Item, '. $wishlist->product->title.' Added to your wishlist.');
}
public function view(){
	return view('product');
}


public function destroy($id)
    {
		
      $wishlists =DB::table('wishlists')->where('id',$id)->delete();
      

      return redirect()->back();
    }
   
}
