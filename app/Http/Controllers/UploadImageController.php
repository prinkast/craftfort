<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use file;
use App\Uploadimage;
use Datatables;
use Illuminate\Support\Facades\Facade;
use Yajra\DataTables\Services\DataTable;

class UploadImageController extends Controller
{
    public function index(Request $request)
    {
         
      if ($request->ajax()) {
            $data = Uploadimage::latest()->get();
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)"     data-id="'.$row->id.'" data-original-title="Edit" form="form-uploadimage" data-toggle="modal" data-target="#myModal"  class="edit btn btn-primary btn-sm editUploadImage">Edit</a>';
						   
   $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteUploadImage">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('backend/uploadimage');
    
      
    }
	
	 public function store(Request $request)
    {
		
		if(isset($request->image_id)){
        $data1=Uploadimage::select('image_name')->where('id',$request->image_id)->first();
		$mnt = $data1->image_name;}
		//var_dump($mnt);
		if($request->image_name!=''){
		   $uploaddir='public/upload';
		   $images = basename($_FILES['image_name']['name']);
		   $temp_name=$_FILES['image_name']['tmp_name'];
           $directory=$uploaddir.'/'.$images;
		 move_uploaded_file($temp_name,$directory );
		
		} 
		else{
			$images= $mnt;
		}

        Uploadimage::updateOrCreate(['id' => $request->image_id],
                ['type'=>$request->type,'status' => $request->status,'image_name' => $images]);        
   
        return response()->json(['success'=>'upload image saved successfully.']); 
    }
	
	 public function edit($id)
    {
		
        $uploadimages = Uploadimage::find($id);
		$uploadimage = json_decode($uploadimages);
		//var_dump($up);die;
		
        return response()->json($uploadimage);
    }
  
   
   
    public function destroy($id)
    {
		//var_dump($id);die;
        Uploadimage::find($id)->delete();
     
        return response()->json(['success'=>'Product deleted successfully.']);
    }
  
}
