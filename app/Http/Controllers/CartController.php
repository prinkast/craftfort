<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Darryldecode\Cart\CartCondition;
use DB;
class CartController extends Controller
{
    public function add(Request $request)
    {
        $userId = Auth::id(); 
        //$item = DB::table('products')->where('id', '=', $request->id)->first();
<<<<<<< HEAD
	    $item =	 DB::table('products')
=======
	$item =	 DB::table('products')
>>>>>>> 9cc800d408ce84b53db52b272c6a37bac30be335
            ->join('product_images', 'products.id', '=', 'product_images.product_id')
            ->select('products.*', 'product_images.image')
            ->where('products.id',$request->id)->first();
			
        $id = $item->id;
        $name = $item->product_name;
        $price = $item->price ;
        $qty = 1;
        $attr['attributes'] =  array('image' => $item->image);
        if(\Cart::session($userId)->add($id, $name, $price, $qty,$attr)){
        	return response()->json([
			    'status' => true,
	            'message' => "item added."
			]);
		}else{
			return response()->json([
			    'status' => false,
	            'message' => "item not added."
			]);
		}  
    }

    public function GetCartProducts(){
        $userId = Auth::id();
        $items = \Cart::session($userId)->getContent();
        $total = \Cart::session($userId)->getTotal();
        $subTotal = \Cart::session($userId)->getSubTotal();
        return view('cart',['items'=>$items,'total_items'=>$items->count(),'total'=>$total]);
    }


    public function removeItem(Request $request){
        $userId = Auth::id();
        if(\Cart::session($userId)->remove($request->id)){

            return response()->json([
                'status' => true,
                'message' => "item removed."
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => "item not removed."
            ]);
        }
    }
	public function updateItem(Request $request){
		
        $userId = Auth::id();
        if(\Cart::session($userId)->update($request->id,['quantity'=>['relative' => false,'value' =>$request->quantity]])){

            return response()->json([
                'status' => true,
                'message' => "item Updated."
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => "item not Updated."
            ]);
        }
    }
}
