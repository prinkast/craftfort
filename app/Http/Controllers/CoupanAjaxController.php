<?php
         
namespace App\Http\Controllers;
          
use App\Category;
use App\Coupon;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Facade;
use Yajra\DataTables\Services\DataTable;
use DB;
class CoupanAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   	 public function index(Request $request)
    {
		
		if ($request->ajax()) {
            $data = Coupon::latest()->get();
			
             return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm" onclick="openCoupan(\''.$row->id.'\',\''.$row->coupon_name.'\',\''.$row->discount.'\')">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCoupan">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('backend/coupanAjax');
	}

   	public function Addcoupan(Request $request){
   		if(DB::table('coupon')->insert([
			['coupon_name' => $request->coupon_name, 'discount' => $request->discount,'status'=>'active']
		])){
			return redirect()->back()->with('message','Successfuly added');
		}else{
			return redirect()->back()->with('message','Something went wrong');
		}
   	}


   	public function Editcoupan(Request $request){
   		if(DB::table('coupon')
            ->where('id', $request->coupan_id)
            ->update(['coupon_name' => $request->coupon_name, 'discount' => $request->discount,'status'=>$request->status])){
   			return redirect()->back()->with('message','Successfuly updated');
   		}else{
   			return redirect()->back()->with('message','Something went wrong');
   		}
    }
	
	
	public function destroy($id)
    {
		
        Coupon::find($id)->delete();
     
        return response()->json(['success'=>'Coupan deleted successfully.']);
    }
	public function edit($id)
    {
        $Coupan = Coupon::find($id);
        return response()->json($Coupan);
    }
   public function store(Request $request)
    {
        Coupon::updateOrCreate(['id' => $request->cat_id],
                ['cat_name' => $request->coupon_name, 'cat_details' => $request->discount]);        
   
        return response()->json(['success'=>'Coupan saved successfully.']);
    }
}