<?php
         
namespace App\Http\Controllers;
          
use App\Category;
use Illuminate\Http\Request;
use DataTables;
use DB;
class CategoryAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		
		if ($request->ajax()) {
            $data = Category::latest()->get();
			
             return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm" onclick="openCat(\''.$row->id.'\',\''.$row->cat_name.'\',\''.$row->cat_details.'\')">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('backend/categoryAjax');
       //return Datatables::of(Category::query())->make(true); */
    
}
	 public function create()
    {
      
        return view('backend/categoryAjax');
    }
	
	public function AddCategory(Request $request){
		if(DB::table('categories')->insert([
			['cat_name' => $request->cat_name, 'cat_details' => $request->cat_details]
		])){
			return redirect()->back()->with('message','Successfuly added');
		}else{
			return redirect()->back()->with('message','Something went wrong');
		}

		
	}

	
	public function destroy($id)
    {
		
        Category::find($id)->delete();
     
        return response()->json(['success'=>'category deleted successfully.']);
    }


	 public function edit($id)
    {
        $category = Category::find($id);
        return response()->json($category);
    }
   public function store(Request $request)
    {
        Category::updateOrCreate(['id' => $request->cat_id],
                ['cat_name' => $request->cat_name, 'cat_details' => $request->cat_details]);        
   
        return response()->json(['success'=>'Product saved successfully.']);
    }
    public function EditCat(Request $request){
   		if(DB::table('categories')
            ->where('id', $request->cat_id)
            ->update(['cat_name' => $request->cat_name, 'cat_details' => $request->cat_details])){
   			return redirect()->back()->with('message','Successfuly added');
   		}else{
   			return redirect()->back()->with('message','Something went wrong');
   		}
    }
	
}