<?php
  
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
   
class Category extends Model
{
    protected $fillable = [
        'cat_name', 'cat_details'
    ];
    public static function CategoryList(){
        return DB::table('categories')
                  
                    ->select('categories.cat_name' ,'categories.id' )
                    ->get();
    }
}