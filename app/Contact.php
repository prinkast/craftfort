<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
	 protected $data=['firstname','lastname','email','address','number','pincode','comment'];
}
