<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
 protected $table = 'coupon';
    protected $fillable = [
        'coupon_name', 'discount','status'
    ];
}
