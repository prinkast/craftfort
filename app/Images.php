<?php
  
namespace App;
  
use Illuminate\Database\Eloquent\Model;
   
class Images extends Model
{
    protected $table = 'upload_images';
    protected $fillable = [
       'image_name', 'type'
    ];
    
}